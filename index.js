const uuid = require('uuid');
const dialogflow = require('dialogflow').v2beta1;
const base64 = require('file-base64');
const util = require('util');
const fs = require('fs');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
const port = process.env.PORT || 3000;
const projectId ="arassistantgsi";

app.post('/api',async function (req, res) {	
    var audioData = req.body.audioData || '';
    if (audioData != ''){
       detectSoundIntent(audioData, function(result) {
   		res.json(result);
		});
    }
});

app.listen(port, function() {
  console.log('Aplicación ejemplo, escuchando el puerto '+ port);
});

async function detectSoundIntent(audioData,callback) {
  const credentials_file_path = './arassistantgsi-21a9592cffe9.json';
  const sessionClient = new dialogflow.SessionsClient({
  projectId,
  keyFilename: credentials_file_path,
  });
  const sessionId = uuid.v4();
  const sessionPath = sessionClient.sessionPath(projectId, sessionId);
	const request = {
  		session: sessionPath,
 		queryInput: {
    		text: {
      			text: audioData,
      			languageCode: "es-ES",
    		},
  		},
  		queryParams: {
    		sentimentAnalysisRequestConfig: {
      		analyzeQueryTextSentiment: true,
    		},
  		},
	};
	const responses = await sessionClient.detectIntent(request);
	console.log('Detected intent');
	const result = responses[0].queryResult;
	console.log(`  Query: ${result.queryText}`);
	console.log(`  Response: ${result.fulfillmentText}`);
	if (result.intent) {
 		console.log(`  Intent: ${result.intent.displayName}`);
	} else {
  		console.log(`  No intent matched.`);
	}
	var sentiment ="";
	if (result.sentimentAnalysisResult) {
 		console.log(`Detected sentiment`);
  		console.log(`  Score: ${result.sentimentAnalysisResult.queryTextSentiment.score}`);
  		console.log(`  Magnitude: ${result.sentimentAnalysisResult.queryTextSentiment.magnitude}`);
  		sentiment = result.sentimentAnalysisResult.queryTextSentiment;
	} else {
		sentiment= "No sentiment Analysis Found";
  		console.log(`No sentiment Analysis Found`);
	}
	var response ='{"dataResponse":"'+result.fulfillmentText+'","emotion":"'+sentiment+'"}';
	callback(response);
}



